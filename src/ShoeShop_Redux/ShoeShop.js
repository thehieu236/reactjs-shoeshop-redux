import React, { Component } from 'react';
import Cart from './Cart';
import DetailShoe from './DetailShoeShop';
import Header from './Header';
import ListShoeShop from './ListShoeShop';

export default class ShoeShop extends Component {
	render() {
		return (
			<>
				<Header />
				<div className="center container">
					<Cart />
					<DetailShoe />

					<ListShoeShop />
				</div>
			</>
		);
	}
}
