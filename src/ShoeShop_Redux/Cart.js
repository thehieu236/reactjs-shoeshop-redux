import React, { Component } from 'react';
import { connect } from 'react-redux';

class Cart extends Component {
	renderTbody = () => {
		return this.props.gioHang.map((item) => {
			return (
				<tr>
					<td>{item.id}</td>
					<td>{item.name}</td>
					<td>{item.price * item.number}</td>
					<td className="units">
						<button
							onClick={() => {
								this.props.handleTangGiamSoLuong(item.id, -1);
							}}
							className="btn minus">
							-
						</button>
						{item.number}
						<button
							onClick={() => {
								this.props.handleTangGiamSoLuong(item.id, +1);
							}}
							className="btn plus">
							+
						</button>
					</td>
					<td>
						<img style={{ width: '80px' }} src={item.image} alt="" />
					</td>
				</tr>
			);
		});
	};
	render() {
		return (
			<table className="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Image</th>
					</tr>
				</thead>
				<tbody>{this.renderTbody()}</tbody>
			</table>
		);
	}
}
let mapStateToProps = (state) => {
	return {
		gioHang: state.shoeReducer.cart,
	};
};

let mapDispatchToProps = (dispatch) => {
	return {
		handleTangGiamSoLuong: (id, soLuong) => {
			let action = {
				type: 'TANG_GIAM_SO_LUONG',
				payload: {
					idShoe: id,
					soLuong: soLuong,
				},
			};
			dispatch(action);
		},
	};
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
