import { Component } from 'react';
import { connect } from 'react-redux';
import {
	ADD_TO_CART,
	CHANGE_DETAIL,
} from '../ShoeShop_Redux/Redux/constants/shoeContant';

class ItemShoesShop extends Component {
	render() {
		let { image, name, description, price } = this.props.data;
		return (
			<div className="item col-3">
				<div className="card">
					<div className="img">
						<img src={image} alt />
					</div>
					<div className="content">
						<div className="title">{name}</div>
						<div className="desc">{description}</div>
						<div className="price">
							Giá:
							<p>{price}$</p>
						</div>
						<button
							onClick={() => {
								this.props.handleAddToCart(this.props.data);
							}}
							className="add">
							Thêm vào giỏ hàng
						</button>
						<button
							onClick={() => {
								this.props.handleChangeDetail(this.props.data);
							}}
							className="detail">
							Chi tiết
						</button>
					</div>
				</div>
			</div>
		);
	}
}

let mapDispatchToProps = (dispatch) => {
	return {
		handleAddToCart: (shoe) => {
			let action = {
				type: ADD_TO_CART,
				payload: shoe,
			};
			dispatch(action);
		},
		handleChangeDetail: (shoe) => {
			let action = {
				type: CHANGE_DETAIL,
				payload: shoe,
			};
			dispatch(action);
		},
	};
};

export default connect(null, mapDispatchToProps)(ItemShoesShop);
