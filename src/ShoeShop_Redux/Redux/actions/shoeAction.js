import { XEM_CHI_TIET } from '../constants/shoeContant';
export const changeDetailAction = (value) => {
	return {
		type: XEM_CHI_TIET,
		payload: value,
	};
};
