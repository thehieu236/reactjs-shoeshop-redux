import React, { Component } from 'react';
import { connect } from 'react-redux';
import ItemShoesShop from './ItemShoesShop';

class ListShoeShop extends Component {
	renderListShoeShop() {
		return this.props.listShoeShop.map((item, index) => {
			return <ItemShoesShop data={item} key={index} />;
		});
	}
	render() {
		return <div className="row">{this.renderListShoeShop()}</div>;
	}
}

let mapStateToProps = (state) => {
	return {
		listShoeShop: state.shoeReducer.shoeArr,
	};
};

export default connect(mapStateToProps)(ListShoeShop);
