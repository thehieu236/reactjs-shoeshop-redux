import './App.css';
import ShoeShop from './ShoeShop_Redux/ShoeShop';

function App() {
	return (
		<div className="App">
			<ShoeShop />
		</div>
	);
}

export default App;
